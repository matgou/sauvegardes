#!/bin/bash
#@****************************************************************************
#@ Author : Mathieu GOULIN (mathieu.goulin@gadz.org)
#@ Organization : ASRALL TP
#@ Licence : GNU/GPL
#@
#@ Description : Script d'entrainement pour manipuler les BDDs
#@
#@
#@****************************************************************************
#@ History :
#@  - Mathieu GOULIN - 2018/10/10 : Initialisation du script
#@****************************************************************************

# Static configuration
cd `dirname $0`
script=`basename "$0" | cut -f1 -d"."`
log_file=`pwd`/$script.log

export WORKDIR=/work
export SAVEDIR=/save
export DBHOST=
export DBPORT=
export DBUSER=
export DB=

#****************************************************************************
# !!! DO NOT MODIFY THIS SECTION !!!
#     GLOBAL FUNCTIONS
#****************************************************************************
# Usage function
function usage () {
    # TODO - Write the good stuff here...
    echo "$0 start                 - Lance une sauvegarde"
    echo "$0 info <sauvegarde_id>  - Affiche les informations d'une sauvegarde"
    echo "$0 version <fichier>     - Affiche les version disponnible d'un fichier"
}

# Help function
function help () {
    grep -e "^#@" $script.sh | sed "s/^#@//"
    usage
    echo
}

# Log function
write_log () {
    log_state=$1
    shift;
    log_txt=$*
    log_date=`date +'%Y/%m/%d %H:%M:%S'`
    case ${log_state} in
        BEG)    chrtra="[START]"      ;;
        CFG)    chrtra="[CONF ERR]"   ;;
        ERR)    chrtra="[ERROR]"      ;;
        END)    chrtra="[END]"        ;;
        INF)    chrtra="[INFO]"       ;;
        *)      chrtra="[ - ]"        ;;
    esac
    echo "$log_date $chrtra : ${log_txt}" | tee -a ${log_file} 2>&1
}

test_rc () {
  if [ "x$1" != "x0" ]
  then
    write_log ERR "Erreur RC=$1"
    exit $1
  fi
}

#****************************************************************************
# MODIFY THIS SECTION
#     BDD FUNCTIONS
#****************************************************************************
getNewSauvegardeId()
{
#  psql -h $DBHOST -p $DBPORT -U $DBUSER -t -c "SELECT nextval('XXXXXX');" $DB
  test_rc $?
}

insertDatabase()
{
  sauvegarde_id=$1
  fichier=$2

  # TODO INSERT into database
  # psql blablabla
  test_rc $?

}

startSauvegarde()
{
  sauvegarde_id=$1

  # TODO INSERT into database
  # psql blablabla
  test_rc $?
}

endSauvegarde()
{
  sauvegarde_id=$1

  # TODO INSERT into database
  # psql blablabla
  test_rc $?
}

#****************************************************************************
# !!! DO NOT MODIFY THIS SECTION !!!
#     SAVE FUNCTIONS
#****************************************************************************

doSave() {
  

  sauvegarde_id=$( getNewSauvegardeId )
  test_rc $?

  startSauvegarde $sauvegarde_id

  # Creation du repertoire save
  mkdir -p $SAVEDIR/$sauvegarde_id 2>/dev/null
  test_rc $?

  # Copie des fichiers dans le repertoire save
  find $WORKDIR -type f | while read fichier
  do
    mkdir -p $(dirname $SAVEDIR/$sauvegarde_id/$fichier) > /dev/null
    test_rc $?

    cp $fichier $SAVEDIR/$sauvegarde_id/$fichier
    test_rc $?

    insertDatabase $sauvegarde_id $fichier
  done

  endSauvegarde $sauvegarde_id

}

printInfo() {
  echo TODO printInfo function
}

printVersion() {
  echo TODO printVersion function
}

#****************************************************************************
# !!! DO NOT MODIFY THIS SECTION !!!
#     MAIN PART
#****************************************************************************
TH=`date +%Y%m%d%H%M%S`
ACTION=$1

case "$ACTION" in
  start)
    doSave
    RC=$?
    test_rc $RC
  ;;
  info)
    sauvegarde_id=$2
    if [ "$sauvegarde_id" != "" ];
    then
      printInfo $sauvegarde_id
      RC=$?
      test_rc $RC
    else
      help
    fi
  ;;
  version)
    fichier=$2
    printVersion $fichier
    RC=$?
    test_rc $RC
  ;;
  *)
    help
    ;;
esac

